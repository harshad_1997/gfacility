import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { TasksComponent } from './pages/tasks/tasks.component';
import { DxButtonModule, DxDataGridModule, DxFormModule, DxListModule, DxPopupModule, DxSelectBoxModule, DxTabsModule, DxTemplateModule, DxTextBoxModule } from 'devextreme-angular';
import { CommonModule } from "@angular/common";

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'tabs',
    component: ProfileComponent,
  },
  {
    path: '**',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), 
    DxDataGridModule,
    DxTabsModule,
    CommonModule,
    DxPopupModule,
    DxButtonModule,
    DxTemplateModule,
    DxSelectBoxModule,
    DxTextBoxModule,
    DxListModule, 
    DxFormModule
  ],
  providers: [],
  exports: [RouterModule],
  declarations: [HomeComponent, ProfileComponent, TasksComponent]
})
export class AppRoutingModule { }
