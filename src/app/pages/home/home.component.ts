import { Component } from '@angular/core';
import { AppInfoService, Product } from "src/app/shared/services";

@Component({
  templateUrl: 'home.component.html',
  styleUrls: [ './home.component.scss' ]
})

export class HomeComponent {
  products: Product[];
  constructor(public appInfoService: AppInfoService) {
    this.products = this.appInfoService.getProducts();
  }

}
