import { Component } from '@angular/core';
import { AppInfoService, Field, Tab, tabFields } from "src/app/shared/services";

@Component({
  templateUrl: 'profile.component.html',
  styleUrls: ['./profile.component.scss']
})

export class ProfileComponent {
  tabs: Tab[];
  tabContent: string;
  index = 0;
  popupVisible = true;
  fields: tabFields;
  labelLocation: string;
  readOnly: boolean;
  showColon: boolean;
  submit = false;
  minColWidth: number;
  colCount: number;
  name: any;
  field_name: any = '';
  field_type: any = 'Numeric';
  addcustomfields = false;
  title: string = 'Cretate new tab';
  width: any;
  field: Field[];
  buttonOptions = {
    text: "Submit",
    useSubmitBehavior: true
  }

  submitText(text) {
    console.log('texxt', text);
  }

  constructor(private service: AppInfoService) {
    this.tabs = this.service.getTabs();
    this.tabContent = this.tabs[0].content;
    this.labelLocation = "top";
    this.readOnly = false;
    this.showColon = true;
    this.minColWidth = 300;
    this.colCount = 2;
    this.fields = this.service.getFields();
    this.field = this.service.getField();
    console.log(this.field, this.fields);
  }
  onDragStart(e) {
    e.itemData = e.fromData[e.fromIndex];
  }

  onAdd(e) {
    e.toData.splice(e.toIndex, 0, e.itemData);
  }

  onRemove(e) {
    e.fromData.splice(e.fromIndex, 1);
  }
  onFormSubmit() {
    console.log('name>>', this.name);
    if (!this.name || this.name == '') {
      this.submit = true;
      return;
    } else {
      this.tabs.splice(this.index, 0, {
        id: this.index + 1,
        text: this.name,
        icon: '',
        content: '',
      });
      this.popupVisible = false;
    }
  }
  onaddCustomFieldsForm() {
    this.addcustomfields = true;
  }
  onFormSubmitOfCustomField() {
    this.field.push({
        id: 0,
        text: this.field_name,
        type: this.field_type
    });
  }
  selectTab(e) {
    this.index = e.itemIndex;
    this.tabContent = this.tabs[e.itemIndex].content;
    console.log('index', this.index);
    if (this.index == this.tabs.length - 1) {
      this.popupVisible = true;
      this.submit = false;
      this.name = '';
    }
  }
}
