import { Injectable } from '@angular/core';

export class Product {
  ID: number;
  Name: string;
  slug: string;
}


let products: Product[] = [{
  ID: 1,
  Name: "Location",
  slug: "location",
}, {
  ID: 2,
  Name: "Organization",
  slug: "organization"
}, {
  ID: 3,
  Name: "Users",
  slug: "users"
}, {
  ID: 4,
  Name: "Catering",
  slug: "catering"
}, {
  ID: 5,
  Name: "Finance",
  slug: "finanace"
}, {
  ID: 6,
  Name: "Events",
  slug: "events"
}
];

export class Tab {
  id: number;
  text: string;
  icon: string;
  content: string;
}

let tabs: Tab[] = [
  {
    id: 0, 
    text: '',
    icon: 'plus',
    content: "Fields"
  }
];

export class Field {
  id: number;
  text: string;
  type: string;
}

let field: Field[] = [];


export class tabFields {
  name: string;
}
 
let fields: tabFields = {
    name: '',
};



@Injectable()
export class AppInfoService {
  constructor() {}

  public get title() {
    return 'GfacilityTask';
  }

  getTabs(): Tab[] {
    return tabs;
  }

  getField(): Field[] {
    return field;
  }

  getFields():tabFields {
    return fields;
  } 

  public getProducts() : Product[] {
    return products;
  }

  public get currentYear() {
    return new Date().getFullYear();
  }
}
